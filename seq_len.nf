// Länge der FASTA Seqeuenzen auslesen
//input FASTA
// Länge der Sequenzen


nextflow.enable.dsl = 2

// Sequenzen splitten in einzelne Dateien
process split_file {
//  publishDir "~/ngsmodule/testprojekt/output", mode = 'copy', overwrite = true
  input:
    path infile
  output:
    path "${infile}.line*" // * Platzhalter 
  script:
    """
    split -l 2 ${infile} -d ${infile}.line
    """
}


// Länge der Sequenzen bestimmen
process length{
  input:
    path infile
  output:
    path "${infile}.laenge"
  script:
    """
    tail -n 1 ${infile} > sequenz
    # < sequenz wc -m > ${infile}.laenge  wc -m => in Standardoutput umleiten und Zeichen zählen
    cat sequenz | tr -d "\n"| wc -m > ${infile}.laenge 
    """
}

// Zusammenfassung in Textdatei in konsekutiven Zeilen
process summary{
  publishDir "${params.outdir}", mode: 'copy', overwrite: true
  input:
    path lenfile
  output:
    path "lenout"
  script:
    """
    cat ${lenfile} > lenout
    cat lenout | paste -sd"+" > sumout
    cat lenout | wc -l
    cat lenout | paste -sd "+" > calc
    cat lenout | wc | >> calc
    cat calc | paste -sd "/" | bc >> calc
    
    """

}

workflow {
// einlesen
  inchannel = channel.fromPath("/home/lisa/ngsmodule/cq-init/data/sequences.fasta")
// FASTA Sequenzen splitten
  splitfiles = split_file(inchannel)
// Längen berechnen
  lengths = length(splitfiles.flatten()) // splitfile => viele Dateien als ein
                                         // Array! flatten -> Einzelchannels
// Zusammenfassung
  ergebnisse = summary(lengths.collect())
}

